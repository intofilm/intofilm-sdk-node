module.exports = function (params) {
  const { stage } = params;
  const definition = require(`./src/sdk.${stage}.js`);

  return definition.newClient(params);
};