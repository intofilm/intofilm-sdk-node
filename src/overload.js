/**
 * @param {object} params
 * @param {array} keys
 */
const parseParametersToObject = (params, keys) => {
  var object = {};

  if (params !== undefined) {
    keys.forEach(key => {
      if (params[key]) {
        object[key] = params[key];
      }
    });
  }

  return object;
};

/**
 * @param {object} params
 * @param {array} keys
 * @param {boolean} ignore
 */
const assertParametersDefined = (params, keys, ignore) => {
  return;
};