const axios = require("./axios.standalone");
/*
CryptoJS v3.1.2
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/
var CryptoJS=CryptoJS||function(h,s){var f={},g=f.lib={},q=function(){},m=g.Base={extend:function(a){q.prototype=this;var c=new q;a&&c.mixIn(a);c.hasOwnProperty("init")||(c.init=function(){c.$super.init.apply(this,arguments)});c.init.prototype=c;c.$super=this;return c},create:function(){var a=this.extend();a.init.apply(a,arguments);return a},init:function(){},mixIn:function(a){for(var c in a)a.hasOwnProperty(c)&&(this[c]=a[c]);a.hasOwnProperty("toString")&&(this.toString=a.toString)},clone:function(){return this.init.prototype.extend(this)}},
r=g.WordArray=m.extend({init:function(a,c){a=this.words=a||[];this.sigBytes=c!=s?c:4*a.length},toString:function(a){return(a||k).stringify(this)},concat:function(a){var c=this.words,d=a.words,b=this.sigBytes;a=a.sigBytes;this.clamp();if(b%4)for(var e=0;e<a;e++)c[b+e>>>2]|=(d[e>>>2]>>>24-8*(e%4)&255)<<24-8*((b+e)%4);else if(65535<d.length)for(e=0;e<a;e+=4)c[b+e>>>2]=d[e>>>2];else c.push.apply(c,d);this.sigBytes+=a;return this},clamp:function(){var a=this.words,c=this.sigBytes;a[c>>>2]&=4294967295<<
32-8*(c%4);a.length=h.ceil(c/4)},clone:function(){var a=m.clone.call(this);a.words=this.words.slice(0);return a},random:function(a){for(var c=[],d=0;d<a;d+=4)c.push(4294967296*h.random()|0);return new r.init(c,a)}}),l=f.enc={},k=l.Hex={stringify:function(a){var c=a.words;a=a.sigBytes;for(var d=[],b=0;b<a;b++){var e=c[b>>>2]>>>24-8*(b%4)&255;d.push((e>>>4).toString(16));d.push((e&15).toString(16))}return d.join("")},parse:function(a){for(var c=a.length,d=[],b=0;b<c;b+=2)d[b>>>3]|=parseInt(a.substr(b,
2),16)<<24-4*(b%8);return new r.init(d,c/2)}},n=l.Latin1={stringify:function(a){var c=a.words;a=a.sigBytes;for(var d=[],b=0;b<a;b++)d.push(String.fromCharCode(c[b>>>2]>>>24-8*(b%4)&255));return d.join("")},parse:function(a){for(var c=a.length,d=[],b=0;b<c;b++)d[b>>>2]|=(a.charCodeAt(b)&255)<<24-8*(b%4);return new r.init(d,c)}},j=l.Utf8={stringify:function(a){try{return decodeURIComponent(escape(n.stringify(a)))}catch(c){throw Error("Malformed UTF-8 data");}},parse:function(a){return n.parse(unescape(encodeURIComponent(a)))}},
u=g.BufferedBlockAlgorithm=m.extend({reset:function(){this._data=new r.init;this._nDataBytes=0},_append:function(a){"string"==typeof a&&(a=j.parse(a));this._data.concat(a);this._nDataBytes+=a.sigBytes},_process:function(a){var c=this._data,d=c.words,b=c.sigBytes,e=this.blockSize,f=b/(4*e),f=a?h.ceil(f):h.max((f|0)-this._minBufferSize,0);a=f*e;b=h.min(4*a,b);if(a){for(var g=0;g<a;g+=e)this._doProcessBlock(d,g);g=d.splice(0,a);c.sigBytes-=b}return new r.init(g,b)},clone:function(){var a=m.clone.call(this);
a._data=this._data.clone();return a},_minBufferSize:0});g.Hasher=u.extend({cfg:m.extend(),init:function(a){this.cfg=this.cfg.extend(a);this.reset()},reset:function(){u.reset.call(this);this._doReset()},update:function(a){this._append(a);this._process();return this},finalize:function(a){a&&this._append(a);return this._doFinalize()},blockSize:16,_createHelper:function(a){return function(c,d){return(new a.init(d)).finalize(c)}},_createHmacHelper:function(a){return function(c,d){return(new t.HMAC.init(a,
d)).finalize(c)}}});var t=f.algo={};return f}(Math);
(function(h){for(var s=CryptoJS,f=s.lib,g=f.WordArray,q=f.Hasher,f=s.algo,m=[],r=[],l=function(a){return 4294967296*(a-(a|0))|0},k=2,n=0;64>n;){var j;a:{j=k;for(var u=h.sqrt(j),t=2;t<=u;t++)if(!(j%t)){j=!1;break a}j=!0}j&&(8>n&&(m[n]=l(h.pow(k,0.5))),r[n]=l(h.pow(k,1/3)),n++);k++}var a=[],f=f.SHA256=q.extend({_doReset:function(){this._hash=new g.init(m.slice(0))},_doProcessBlock:function(c,d){for(var b=this._hash.words,e=b[0],f=b[1],g=b[2],j=b[3],h=b[4],m=b[5],n=b[6],q=b[7],p=0;64>p;p++){if(16>p)a[p]=
c[d+p]|0;else{var k=a[p-15],l=a[p-2];a[p]=((k<<25|k>>>7)^(k<<14|k>>>18)^k>>>3)+a[p-7]+((l<<15|l>>>17)^(l<<13|l>>>19)^l>>>10)+a[p-16]}k=q+((h<<26|h>>>6)^(h<<21|h>>>11)^(h<<7|h>>>25))+(h&m^~h&n)+r[p]+a[p];l=((e<<30|e>>>2)^(e<<19|e>>>13)^(e<<10|e>>>22))+(e&f^e&g^f&g);q=n;n=m;m=h;h=j+k|0;j=g;g=f;f=e;e=k+l|0}b[0]=b[0]+e|0;b[1]=b[1]+f|0;b[2]=b[2]+g|0;b[3]=b[3]+j|0;b[4]=b[4]+h|0;b[5]=b[5]+m|0;b[6]=b[6]+n|0;b[7]=b[7]+q|0},_doFinalize:function(){var a=this._data,d=a.words,b=8*this._nDataBytes,e=8*a.sigBytes;
d[e>>>5]|=128<<24-e%32;d[(e+64>>>9<<4)+14]=h.floor(b/4294967296);d[(e+64>>>9<<4)+15]=b;a.sigBytes=4*d.length;this._process();return this._hash},clone:function(){var a=q.clone.call(this);a._hash=this._hash.clone();return a}});s.SHA256=q._createHelper(f);s.HmacSHA256=q._createHmacHelper(f)})(Math);
(function(){var h=CryptoJS,s=h.enc.Utf8;h.algo.HMAC=h.lib.Base.extend({init:function(f,g){f=this._hasher=new f.init;"string"==typeof g&&(g=s.parse(g));var h=f.blockSize,m=4*h;g.sigBytes>m&&(g=f.finalize(g));g.clamp();for(var r=this._oKey=g.clone(),l=this._iKey=g.clone(),k=r.words,n=l.words,j=0;j<h;j++)k[j]^=1549556828,n[j]^=909522486;r.sigBytes=l.sigBytes=m;this.reset()},reset:function(){var f=this._hasher;f.reset();f.update(this._iKey)},update:function(f){this._hasher.update(f);return this},finalize:function(f){var g=
this._hasher;f=g.finalize(f);g.reset();return g.finalize(this._oKey.clone().concat(f))}})})();

/*
CryptoJS v3.1.2
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/
var CryptoJS=CryptoJS||function(h,s){var f={},t=f.lib={},g=function(){},j=t.Base={extend:function(a){g.prototype=this;var c=new g;a&&c.mixIn(a);c.hasOwnProperty("init")||(c.init=function(){c.$super.init.apply(this,arguments)});c.init.prototype=c;c.$super=this;return c},create:function(){var a=this.extend();a.init.apply(a,arguments);return a},init:function(){},mixIn:function(a){for(var c in a)a.hasOwnProperty(c)&&(this[c]=a[c]);a.hasOwnProperty("toString")&&(this.toString=a.toString)},clone:function(){return this.init.prototype.extend(this)}},
q=t.WordArray=j.extend({init:function(a,c){a=this.words=a||[];this.sigBytes=c!=s?c:4*a.length},toString:function(a){return(a||u).stringify(this)},concat:function(a){var c=this.words,d=a.words,b=this.sigBytes;a=a.sigBytes;this.clamp();if(b%4)for(var e=0;e<a;e++)c[b+e>>>2]|=(d[e>>>2]>>>24-8*(e%4)&255)<<24-8*((b+e)%4);else if(65535<d.length)for(e=0;e<a;e+=4)c[b+e>>>2]=d[e>>>2];else c.push.apply(c,d);this.sigBytes+=a;return this},clamp:function(){var a=this.words,c=this.sigBytes;a[c>>>2]&=4294967295<<
32-8*(c%4);a.length=h.ceil(c/4)},clone:function(){var a=j.clone.call(this);a.words=this.words.slice(0);return a},random:function(a){for(var c=[],d=0;d<a;d+=4)c.push(4294967296*h.random()|0);return new q.init(c,a)}}),v=f.enc={},u=v.Hex={stringify:function(a){var c=a.words;a=a.sigBytes;for(var d=[],b=0;b<a;b++){var e=c[b>>>2]>>>24-8*(b%4)&255;d.push((e>>>4).toString(16));d.push((e&15).toString(16))}return d.join("")},parse:function(a){for(var c=a.length,d=[],b=0;b<c;b+=2)d[b>>>3]|=parseInt(a.substr(b,
2),16)<<24-4*(b%8);return new q.init(d,c/2)}},k=v.Latin1={stringify:function(a){var c=a.words;a=a.sigBytes;for(var d=[],b=0;b<a;b++)d.push(String.fromCharCode(c[b>>>2]>>>24-8*(b%4)&255));return d.join("")},parse:function(a){for(var c=a.length,d=[],b=0;b<c;b++)d[b>>>2]|=(a.charCodeAt(b)&255)<<24-8*(b%4);return new q.init(d,c)}},l=v.Utf8={stringify:function(a){try{return decodeURIComponent(escape(k.stringify(a)))}catch(c){throw Error("Malformed UTF-8 data");}},parse:function(a){return k.parse(unescape(encodeURIComponent(a)))}},
x=t.BufferedBlockAlgorithm=j.extend({reset:function(){this._data=new q.init;this._nDataBytes=0},_append:function(a){"string"==typeof a&&(a=l.parse(a));this._data.concat(a);this._nDataBytes+=a.sigBytes},_process:function(a){var c=this._data,d=c.words,b=c.sigBytes,e=this.blockSize,f=b/(4*e),f=a?h.ceil(f):h.max((f|0)-this._minBufferSize,0);a=f*e;b=h.min(4*a,b);if(a){for(var m=0;m<a;m+=e)this._doProcessBlock(d,m);m=d.splice(0,a);c.sigBytes-=b}return new q.init(m,b)},clone:function(){var a=j.clone.call(this);
a._data=this._data.clone();return a},_minBufferSize:0});t.Hasher=x.extend({cfg:j.extend(),init:function(a){this.cfg=this.cfg.extend(a);this.reset()},reset:function(){x.reset.call(this);this._doReset()},update:function(a){this._append(a);this._process();return this},finalize:function(a){a&&this._append(a);return this._doFinalize()},blockSize:16,_createHelper:function(a){return function(c,d){return(new a.init(d)).finalize(c)}},_createHmacHelper:function(a){return function(c,d){return(new w.HMAC.init(a,
d)).finalize(c)}}});var w=f.algo={};return f}(Math);
(function(h){for(var s=CryptoJS,f=s.lib,t=f.WordArray,g=f.Hasher,f=s.algo,j=[],q=[],v=function(a){return 4294967296*(a-(a|0))|0},u=2,k=0;64>k;){var l;a:{l=u;for(var x=h.sqrt(l),w=2;w<=x;w++)if(!(l%w)){l=!1;break a}l=!0}l&&(8>k&&(j[k]=v(h.pow(u,0.5))),q[k]=v(h.pow(u,1/3)),k++);u++}var a=[],f=f.SHA256=g.extend({_doReset:function(){this._hash=new t.init(j.slice(0))},_doProcessBlock:function(c,d){for(var b=this._hash.words,e=b[0],f=b[1],m=b[2],h=b[3],p=b[4],j=b[5],k=b[6],l=b[7],n=0;64>n;n++){if(16>n)a[n]=
c[d+n]|0;else{var r=a[n-15],g=a[n-2];a[n]=((r<<25|r>>>7)^(r<<14|r>>>18)^r>>>3)+a[n-7]+((g<<15|g>>>17)^(g<<13|g>>>19)^g>>>10)+a[n-16]}r=l+((p<<26|p>>>6)^(p<<21|p>>>11)^(p<<7|p>>>25))+(p&j^~p&k)+q[n]+a[n];g=((e<<30|e>>>2)^(e<<19|e>>>13)^(e<<10|e>>>22))+(e&f^e&m^f&m);l=k;k=j;j=p;p=h+r|0;h=m;m=f;f=e;e=r+g|0}b[0]=b[0]+e|0;b[1]=b[1]+f|0;b[2]=b[2]+m|0;b[3]=b[3]+h|0;b[4]=b[4]+p|0;b[5]=b[5]+j|0;b[6]=b[6]+k|0;b[7]=b[7]+l|0},_doFinalize:function(){var a=this._data,d=a.words,b=8*this._nDataBytes,e=8*a.sigBytes;
d[e>>>5]|=128<<24-e%32;d[(e+64>>>9<<4)+14]=h.floor(b/4294967296);d[(e+64>>>9<<4)+15]=b;a.sigBytes=4*d.length;this._process();return this._hash},clone:function(){var a=g.clone.call(this);a._hash=this._hash.clone();return a}});s.SHA256=g._createHelper(f);s.HmacSHA256=g._createHmacHelper(f)})(Math);

/*
CryptoJS v3.1.2
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/
(function () {
    // Shortcuts
    var C = CryptoJS;
    var C_lib = C.lib;
    var Base = C_lib.Base;
    var C_enc = C.enc;
    var Utf8 = C_enc.Utf8;
    var C_algo = C.algo;

    /**
     * HMAC algorithm.
     */
    var HMAC = C_algo.HMAC = Base.extend({
        /**
         * Initializes a newly created HMAC.
         *
         * @param {Hasher} hasher The hash algorithm to use.
         * @param {WordArray|string} key The secret key.
         *
         * @example
         *
         *     var hmacHasher = CryptoJS.algo.HMAC.create(CryptoJS.algo.SHA256, key);
         */
        init: function (hasher, key) {
            // Init hasher
            hasher = this._hasher = new hasher.init();

            // Convert string to WordArray, else assume WordArray already
            if (typeof key == 'string') {
                key = Utf8.parse(key);
            }

            // Shortcuts
            var hasherBlockSize = hasher.blockSize;
            var hasherBlockSizeBytes = hasherBlockSize * 4;

            // Allow arbitrary length keys
            if (key.sigBytes > hasherBlockSizeBytes) {
                key = hasher.finalize(key);
            }

            // Clamp excess bits
            key.clamp();

            // Clone key for inner and outer pads
            var oKey = this._oKey = key.clone();
            var iKey = this._iKey = key.clone();

            // Shortcuts
            var oKeyWords = oKey.words;
            var iKeyWords = iKey.words;

            // XOR keys with pad constants
            for (var i = 0; i < hasherBlockSize; i++) {
                oKeyWords[i] ^= 0x5c5c5c5c;
                iKeyWords[i] ^= 0x36363636;
            }
            oKey.sigBytes = iKey.sigBytes = hasherBlockSizeBytes;

            // Set initial values
            this.reset();
        },

        /**
         * Resets this HMAC to its initial state.
         *
         * @example
         *
         *     hmacHasher.reset();
         */
        reset: function () {
            // Shortcut
            var hasher = this._hasher;

            // Reset
            hasher.reset();
            hasher.update(this._iKey);
        },

        /**
         * Updates this HMAC with a message.
         *
         * @param {WordArray|string} messageUpdate The message to append.
         *
         * @return {HMAC} This HMAC instance.
         *
         * @example
         *
         *     hmacHasher.update('message');
         *     hmacHasher.update(wordArray);
         */
        update: function (messageUpdate) {
            this._hasher.update(messageUpdate);

            // Chainable
            return this;
        },

        /**
         * Finalizes the HMAC computation.
         * Note that the finalize operation is effectively a destructive, read-once operation.
         *
         * @param {WordArray|string} messageUpdate (Optional) A final message update.
         *
         * @return {WordArray} The HMAC.
         *
         * @example
         *
         *     var hmac = hmacHasher.finalize();
         *     var hmac = hmacHasher.finalize('message');
         *     var hmac = hmacHasher.finalize(wordArray);
         */
        finalize: function (messageUpdate) {
            // Shortcut
            var hasher = this._hasher;

            // Compute HMAC
            var innerHash = hasher.finalize(messageUpdate);
            hasher.reset();
            var hmac = hasher.finalize(this._oKey.clone().concat(innerHash));

            return hmac;
        }
    });
}());

/*
CryptoJS v3.1.2
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/
(function () {
    // Shortcuts
    var C = CryptoJS;
    var C_lib = C.lib;
    var WordArray = C_lib.WordArray;
    var C_enc = C.enc;

    /**
     * Base64 encoding strategy.
     */
    var Base64 = C_enc.Base64 = {
        /**
         * Converts a word array to a Base64 string.
         *
         * @param {WordArray} wordArray The word array.
         *
         * @return {string} The Base64 string.
         *
         * @static
         *
         * @example
         *
         *     var base64String = CryptoJS.enc.Base64.stringify(wordArray);
         */
        stringify: function (wordArray) {
            // Shortcuts
            var words = wordArray.words;
            var sigBytes = wordArray.sigBytes;
            var map = this._map;

            // Clamp excess bits
            wordArray.clamp();

            // Convert
            var base64Chars = [];
            for (var i = 0; i < sigBytes; i += 3) {
                var byte1 = (words[i >>> 2]       >>> (24 - (i % 4) * 8))       & 0xff;
                var byte2 = (words[(i + 1) >>> 2] >>> (24 - ((i + 1) % 4) * 8)) & 0xff;
                var byte3 = (words[(i + 2) >>> 2] >>> (24 - ((i + 2) % 4) * 8)) & 0xff;

                var triplet = (byte1 << 16) | (byte2 << 8) | byte3;

                for (var j = 0; (j < 4) && (i + j * 0.75 < sigBytes); j++) {
                    base64Chars.push(map.charAt((triplet >>> (6 * (3 - j))) & 0x3f));
                }
            }

            // Add padding
            var paddingChar = map.charAt(64);
            if (paddingChar) {
                while (base64Chars.length % 4) {
                    base64Chars.push(paddingChar);
                }
            }

            return base64Chars.join('');
        },

        /**
         * Converts a Base64 string to a word array.
         *
         * @param {string} base64Str The Base64 string.
         *
         * @return {WordArray} The word array.
         *
         * @static
         *
         * @example
         *
         *     var wordArray = CryptoJS.enc.Base64.parse(base64String);
         */
        parse: function (base64Str) {
            // Shortcuts
            var base64StrLength = base64Str.length;
            var map = this._map;

            // Ignore padding
            var paddingChar = map.charAt(64);
            if (paddingChar) {
                var paddingIndex = base64Str.indexOf(paddingChar);
                if (paddingIndex != -1) {
                    base64StrLength = paddingIndex;
                }
            }

            // Convert
            var words = [];
            var nBytes = 0;
            for (var i = 0; i < base64StrLength; i++) {
                if (i % 4) {
                    var bits1 = map.indexOf(base64Str.charAt(i - 1)) << ((i % 4) * 2);
                    var bits2 = map.indexOf(base64Str.charAt(i)) >>> (6 - (i % 4) * 2);
                    words[nBytes >>> 2] |= (bits1 | bits2) << (24 - (nBytes % 4) * 8);
                    nBytes++;
                }
            }

            return WordArray.create(words, nBytes);
        },

        _map: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/='
    };
}());

/*
 UriTemplates Template Processor - Version: @VERSION - Dated: @DATE
 (c) marc.portier@gmail.com - 2011-2012
 Licensed under APLv2 (http://opensource.org/licenses/Apache-2.0)
 */

;
var uritemplate = (function() {

// Below are the functions we originally used from jQuery.
// The implementations below are often more naive then what is inside jquery, but they suffice for our needs.

    function isFunction(fn) {
        return typeof fn == 'function';
    }

    function isEmptyObject (obj) {
        for(var name in obj){
            return false;
        }
        return true;
    }

    function extend(base, newprops) {
        for (var name in newprops) {
            base[name] = newprops[name];
        }
        return base;
    }

    /**
     * Create a runtime cache around retrieved values from the context.
     * This allows for dynamic (function) results to be kept the same for multiple
     * occuring expansions within one template.
     * Note: Uses key-value tupples to be able to cache null values as well.
     */
        //TODO move this into prep-processing
    function CachingContext(context) {
        this.raw = context;
        this.cache = {};
    }
    CachingContext.prototype.get = function(key) {
        var val = this.lookupRaw(key);
        var result = val;

        if (isFunction(val)) { // check function-result-cache
            var tupple = this.cache[key];
            if (tupple !== null && tupple !== undefined) {
                result = tupple.val;
            } else {
                result = val(this.raw);
                this.cache[key] = {key: key, val: result};
                // NOTE: by storing tupples we make sure a null return is validly consistent too in expansions
            }
        }
        return result;
    };

    CachingContext.prototype.lookupRaw = function(key) {
        return CachingContext.lookup(this, this.raw, key);
    };

    CachingContext.lookup = function(me, context, key) {
        var result = context[key];
        if (result !== undefined) {
            return result;
        } else {
            var keyparts = key.split('.');
            var i = 0, keysplits = keyparts.length - 1;
            for (i = 0; i<keysplits; i++) {
                var leadKey = keyparts.slice(0, keysplits - i).join('.');
                var trailKey = keyparts.slice(-i-1).join('.');
                var leadContext = context[leadKey];
                if (leadContext !== undefined) {
                    return CachingContext.lookup(me, leadContext, trailKey);
                }
            }
            return undefined;
        }
    };


    function UriTemplate(set) {
        this.set = set;
    }

    UriTemplate.prototype.expand = function(context) {
        var cache = new CachingContext(context);
        var res = "";
        var i = 0, cnt = this.set.length;
        for (i = 0; i<cnt; i++ ) {
            res += this.set[i].expand(cache);
        }
        return res;
    };

//TODO: change since draft-0.6 about characters in literals
    /* extract:
     The characters outside of expressions in a URI Template string are intended to be copied literally to the URI-reference if the character is allowed in a URI (reserved / unreserved / pct-encoded) or, if not allowed, copied to the URI-reference in its UTF-8 pct-encoded form.
     */
    function Literal(txt ) {
        this.txt = txt;
    }

    Literal.prototype.expand = function() {
        return this.txt;
    };



    var RESERVEDCHARS_RE = new RegExp("[:/?#\\[\\]@!$&()*+,;=']","g");
    function encodeNormal(val) {
        return encodeURIComponent(val).replace(RESERVEDCHARS_RE, function(s) {return escape(s);} );
    }

//var SELECTEDCHARS_RE = new RegExp("[]","g");
    function encodeReserved(val) {
        //return encodeURI(val).replace(SELECTEDCHARS_RE, function(s) {return escape(s)} );
        return encodeURI(val); // no need for additional replace if selected-chars is empty
    }


    function addUnNamed(name, key, val) {
        return key + (key.length > 0 ? "=" : "") + val;
    }

    function addNamed(name, key, val, noName) {
        noName = noName || false;
        if (noName) { name = ""; }

        if (!key || key.length === 0)  {
            key = name;
        }
        return key + (key.length > 0 ? "=" : "") + val;
    }

    function addLabeled(name, key, val, noName) {
        noName = noName || false;
        if (noName) { name = ""; }

        if (!key || key.length === 0)  {
            key = name;
        }
        return key + (key.length > 0 && val ? "=" : "") + val;
    }


    var simpleConf = {
        prefix : "",     joiner : ",",     encode : encodeNormal,    builder : addUnNamed
    };
    var reservedConf = {
        prefix : "",     joiner : ",",     encode : encodeReserved,  builder : addUnNamed
    };
    var fragmentConf = {
        prefix : "#",    joiner : ",",     encode : encodeReserved,  builder : addUnNamed
    };
    var pathParamConf = {
        prefix : ";",    joiner : ";",     encode : encodeNormal,    builder : addLabeled
    };
    var formParamConf = {
        prefix : "?",    joiner : "&",     encode : encodeNormal,    builder : addNamed
    };
    var formContinueConf = {
        prefix : "&",    joiner : "&",     encode : encodeNormal,    builder : addNamed
    };
    var pathHierarchyConf = {
        prefix : "/",    joiner : "/",     encode : encodeNormal,    builder : addUnNamed
    };
    var labelConf = {
        prefix : ".",    joiner : ".",     encode : encodeNormal,    builder : addUnNamed
    };


    function Expression(conf, vars ) {
        extend(this, conf);
        this.vars = vars;
    }

    Expression.build = function(ops, vars) {
        var conf;
        switch(ops) {
            case ''  : conf = simpleConf; break;
            case '+' : conf = reservedConf; break;
            case '#' : conf = fragmentConf; break;
            case ';' : conf = pathParamConf; break;
            case '?' : conf = formParamConf; break;
            case '&' : conf = formContinueConf; break;
            case '/' : conf = pathHierarchyConf; break;
            case '.' : conf = labelConf; break;
            default  : throw "Unexpected operator: '"+ops+"'";
        }
        return new Expression(conf, vars);
    };

    Expression.prototype.expand = function(context) {
        var joiner = this.prefix;
        var nextjoiner = this.joiner;
        var buildSegment = this.builder;
        var res = "";
        var i = 0, cnt = this.vars.length;

        for (i = 0 ; i< cnt; i++) {
            var varspec = this.vars[i];
            varspec.addValues(context, this.encode, function(key, val, noName) {
                var segm = buildSegment(varspec.name, key, val, noName);
                if (segm !== null && segm !== undefined) {
                    res += joiner + segm;
                    joiner = nextjoiner;
                }
            });
        }
        return res;
    };



    var UNBOUND = {};

    /**
     * Helper class to help grow a string of (possibly encoded) parts until limit is reached
     */
    function Buffer(limit) {
        this.str = "";
        if (limit === UNBOUND) {
            this.appender = Buffer.UnboundAppend;
        } else {
            this.len = 0;
            this.limit = limit;
            this.appender = Buffer.BoundAppend;
        }
    }

    Buffer.prototype.append = function(part, encoder) {
        return this.appender(this, part, encoder);
    };

    Buffer.UnboundAppend = function(me, part, encoder) {
        part = encoder ? encoder(part) : part;
        me.str += part;
        return me;
    };

    Buffer.BoundAppend = function(me, part, encoder) {
        part = part.substring(0, me.limit - me.len);
        me.len += part.length;

        part = encoder ? encoder(part) : part;
        me.str += part;
        return me;
    };


    function arrayToString(arr, encoder, maxLength) {
        var buffer = new Buffer(maxLength);
        var joiner = "";

        var i = 0, cnt = arr.length;
        for (i=0; i<cnt; i++) {
            if (arr[i] !== null && arr[i] !== undefined) {
                buffer.append(joiner).append(arr[i], encoder);
                joiner = ",";
            }
        }
        return buffer.str;
    }

    function objectToString(obj, encoder, maxLength) {
        var buffer = new Buffer(maxLength);
        var joiner = "";
        var k;

        for (k in obj) {
            if (obj.hasOwnProperty(k) ) {
                if (obj[k] !== null && obj[k] !== undefined) {
                    buffer.append(joiner + k + ',').append(obj[k], encoder);
                    joiner = ",";
                }
            }
        }
        return buffer.str;
    }


    function simpleValueHandler(me, val, valprops, encoder, adder) {
        var result;

        if (valprops.isArr) {
            result = arrayToString(val, encoder, me.maxLength);
        } else if (valprops.isObj) {
            result = objectToString(val, encoder, me.maxLength);
        } else {
            var buffer = new Buffer(me.maxLength);
            result = buffer.append(val, encoder).str;
        }

        adder("", result);
    }

    function explodeValueHandler(me, val, valprops, encoder, adder) {
        if (valprops.isArr) {
            var i = 0, cnt = val.length;
            for (i = 0; i<cnt; i++) {
                adder("", encoder(val[i]) );
            }
        } else if (valprops.isObj) {
            var k;
            for (k in val) {
                if (val.hasOwnProperty(k)) {
                    adder(k, encoder(val[k]) );
                }
            }
        } else { // explode-requested, but single value
            adder("", encoder(val));
        }
    }

    function valueProperties(val) {
        var isArr = false;
        var isObj = false;
        var isUndef = true;  //note: "" is empty but not undef

        if (val !== null && val !== undefined) {
            isArr = (val.constructor === Array);
            isObj = (val.constructor === Object);
            isUndef = (isArr && val.length === 0) || (isObj && isEmptyObject(val));
        }

        return {isArr: isArr, isObj: isObj, isUndef: isUndef};
    }


    function VarSpec (name, vhfn, nums) {
        this.name = unescape(name);
        this.valueHandler = vhfn;
        this.maxLength = nums;
    }


    VarSpec.build = function(name, expl, part, nums) {
        var valueHandler, valueModifier;

        if (!!expl) { //interprete as boolean
            valueHandler = explodeValueHandler;
        } else {
            valueHandler = simpleValueHandler;
        }

        if (!part) {
            nums = UNBOUND;
        }

        return new VarSpec(name, valueHandler, nums);
    };


    VarSpec.prototype.addValues = function(context, encoder, adder) {
        var val = context.get(this.name);
        var valprops = valueProperties(val);
        if (valprops.isUndef) { return; } // ignore empty values
        this.valueHandler(this, val, valprops, encoder, adder);
    };



//----------------------------------------------parsing logic
// How each varspec should look like
    var VARSPEC_RE=/([^*:]*)((\*)|(:)([0-9]+))?/;

    var match2varspec = function(m) {
        var name = m[1];
        var expl = m[3];
        var part = m[4];
        var nums = parseInt(m[5], 10);

        return VarSpec.build(name, expl, part, nums);
    };


// Splitting varspecs in list with:
    var LISTSEP=",";

// How each template should look like
    var TEMPL_RE=/(\{([+#.;?&\/])?(([^.*:,{}|@!=$()][^*:,{}$()]*)(\*|:([0-9]+))?(,([^.*:,{}][^*:,{}]*)(\*|:([0-9]+))?)*)\})/g;
// Note: reserved operators: |!@ are left out of the regexp in order to make those templates degrade into literals
// (as expected by the spec - see tests.html "reserved operators")


    var match2expression = function(m) {
        var expr = m[0];
        var ops = m[2] || '';
        var vars = m[3].split(LISTSEP);
        var i = 0, len = vars.length;
        for (i = 0; i<len; i++) {
            var match;
            if ( (match = vars[i].match(VARSPEC_RE)) === null) {
                throw "unexpected parse error in varspec: " + vars[i];
            }
            vars[i] = match2varspec(match);
        }

        return Expression.build(ops, vars);
    };


    var pushLiteralSubstr = function(set, src, from, to) {
        if (from < to) {
            var literal = src.substr(from, to - from);
            set.push(new Literal(literal));
        }
    };

    var parse = function(str) {
        var lastpos = 0;
        var comp = [];

        var match;
        var pattern = TEMPL_RE;
        pattern.lastIndex = 0; // just to be sure
        while ((match = pattern.exec(str)) !== null) {
            var newpos = match.index;
            pushLiteralSubstr(comp, str, lastpos, newpos);

            comp.push(match2expression(match));
            lastpos = pattern.lastIndex;
        }
        pushLiteralSubstr(comp, str, lastpos, str.length);

        return new UriTemplate(comp);
    };


//-------------------------------------------comments and ideas

//TODO: consider building cache of previously parsed uris or even parsed expressions?

    return parse;

}());

/*
 * Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
 
var apiGateway = apiGateway || {};
apiGateway.core = apiGateway.core || {};

apiGateway.core.sigV4ClientFactory = {};
apiGateway.core.sigV4ClientFactory.newClient = function (config) {
    var AWS_SHA_256 = 'AWS4-HMAC-SHA256';
    var AWS4_REQUEST = 'aws4_request';
    var AWS4 = 'AWS4';
    var X_AMZ_DATE = 'x-amz-date';
    var X_AMZ_SECURITY_TOKEN = 'x-amz-security-token';
    var HOST = 'host';
    var AUTHORIZATION = 'Authorization';

    function hash(value) {
        return CryptoJS.SHA256(value);
    }

    function hexEncode(value) {
        return value.toString(CryptoJS.enc.Hex);
    }

    function hmac(secret, value) {
        return CryptoJS.HmacSHA256(value, secret, {asBytes: true});
    }

    function buildCanonicalRequest(method, path, queryParams, headers, payload) {
        return method + '\n' +
            buildCanonicalUri(path) + '\n' +
            buildCanonicalQueryString(queryParams) + '\n' +
            buildCanonicalHeaders(headers) + '\n' +
            buildCanonicalSignedHeaders(headers) + '\n' +
            hexEncode(hash(payload));
    }

    function hashCanonicalRequest(request) {
        return hexEncode(hash(request));
    }

    function buildCanonicalUri(uri) {
        return encodeURI(uri);
    }

    function buildCanonicalQueryString(queryParams) {
        if (Object.keys(queryParams).length < 1) {
            return '';
        }

        var sortedQueryParams = [];
        for (var property in queryParams) {
            if (queryParams.hasOwnProperty(property)) {
                sortedQueryParams.push(property);
            }
        }
        sortedQueryParams.sort();

        var canonicalQueryString = '';
        for (var i = 0; i < sortedQueryParams.length; i++) {
            canonicalQueryString += sortedQueryParams[i] + '=' + fixedEncodeURIComponent(queryParams[sortedQueryParams[i]]) + '&';
        }
        return canonicalQueryString.substr(0, canonicalQueryString.length - 1);
    }

    function fixedEncodeURIComponent (str) {
      return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
        return '%' + c.charCodeAt(0).toString(16).toUpperCase();
      });
    }

    function buildCanonicalHeaders(headers) {
        var canonicalHeaders = '';
        var sortedKeys = [];
        for (var property in headers) {
            if (headers.hasOwnProperty(property)) {
                sortedKeys.push(property);
            }
        }
        sortedKeys.sort();

        for (var i = 0; i < sortedKeys.length; i++) {
            canonicalHeaders += sortedKeys[i].toLowerCase() + ':' + headers[sortedKeys[i]] + '\n';
        }
        return canonicalHeaders;
    }

    function buildCanonicalSignedHeaders(headers) {
        var sortedKeys = [];
        for (var property in headers) {
            if (headers.hasOwnProperty(property)) {
                sortedKeys.push(property.toLowerCase());
            }
        }
        sortedKeys.sort();

        return sortedKeys.join(';');
    }

    function buildStringToSign(datetime, credentialScope, hashedCanonicalRequest) {
        return AWS_SHA_256 + '\n' +
            datetime + '\n' +
            credentialScope + '\n' +
            hashedCanonicalRequest;
    }

    function buildCredentialScope(datetime, region, service) {
        return datetime.substr(0, 8) + '/' + region + '/' + service + '/' + AWS4_REQUEST
    }

    function calculateSigningKey(secretKey, datetime, region, service) {
        return hmac(hmac(hmac(hmac(AWS4 + secretKey, datetime.substr(0, 8)), region), service), AWS4_REQUEST);
    }

    function calculateSignature(key, stringToSign) {
        return hexEncode(hmac(key, stringToSign));
    }

    function buildAuthorizationHeader(accessKey, credentialScope, headers, signature) {
        return AWS_SHA_256 + ' Credential=' + accessKey + '/' + credentialScope + ', SignedHeaders=' + buildCanonicalSignedHeaders(headers) + ', Signature=' + signature;
    }

    var awsSigV4Client = { };
    if(config.accessKey === undefined || config.secretKey === undefined) {
        return awsSigV4Client;
    }
    awsSigV4Client.accessKey = apiGateway.core.utils.assertDefined(config.accessKey, 'accessKey');
    awsSigV4Client.secretKey = apiGateway.core.utils.assertDefined(config.secretKey, 'secretKey');
    awsSigV4Client.sessionToken = config.sessionToken;
    awsSigV4Client.serviceName = apiGateway.core.utils.assertDefined(config.serviceName, 'serviceName');
    awsSigV4Client.region = apiGateway.core.utils.assertDefined(config.region, 'region');
    awsSigV4Client.endpoint = apiGateway.core.utils.assertDefined(config.endpoint, 'endpoint');

    awsSigV4Client.makeRequest = function (request) {
        var verb = apiGateway.core.utils.assertDefined(request.verb, 'verb');
        var path = apiGateway.core.utils.assertDefined(request.path, 'path');
        var queryParams = apiGateway.core.utils.copy(request.queryParams);
        if (queryParams === undefined) {
            queryParams = {};
        }
        var headers = apiGateway.core.utils.copy(request.headers);
        if (headers === undefined) {
            headers = {};
        }

        //If the user has not specified an override for Content type the use default
        if(headers['Content-Type'] === undefined) {
            headers['Content-Type'] = config.defaultContentType;
        }

        //If the user has not specified an override for Accept type the use default
        if(headers['Accept'] === undefined) {
            headers['Accept'] = config.defaultAcceptType;
        }

        var body = apiGateway.core.utils.copy(request.body);
        if (body === undefined || verb === 'GET') { // override request body and set to empty when signing GET requests
            body = '';
        }  else {
            body = JSON.stringify(body);
        }

        //If there is no body remove the content-type header so it is not included in SigV4 calculation
        if(body === '' || body === undefined || body === null) {
            delete headers['Content-Type'];
        }

        var datetime = new Date().toISOString().replace(/\.\d{3}Z$/, 'Z').replace(/[:\-]|\.\d{3}/g, '');
        headers[X_AMZ_DATE] = datetime;
        var parser = document.createElement('a');
        parser.href = awsSigV4Client.endpoint;
        headers[HOST] = parser.hostname;

        var canonicalRequest = buildCanonicalRequest(verb, path, queryParams, headers, body);
        var hashedCanonicalRequest = hashCanonicalRequest(canonicalRequest);
        var credentialScope = buildCredentialScope(datetime, awsSigV4Client.region, awsSigV4Client.serviceName);
        var stringToSign = buildStringToSign(datetime, credentialScope, hashedCanonicalRequest);
        var signingKey = calculateSigningKey(awsSigV4Client.secretKey, datetime, awsSigV4Client.region, awsSigV4Client.serviceName);
        var signature = calculateSignature(signingKey, stringToSign);
        headers[AUTHORIZATION] = buildAuthorizationHeader(awsSigV4Client.accessKey, credentialScope, headers, signature);
        if(awsSigV4Client.sessionToken !== undefined && awsSigV4Client.sessionToken !== '') {
            headers[X_AMZ_SECURITY_TOKEN] = awsSigV4Client.sessionToken;
        }
        delete headers[HOST];

        var url = config.endpoint + path;
        var queryString = buildCanonicalQueryString(queryParams);
        if (queryString != '') {
            url += '?' + queryString;
        }

        //Need to re-attach Content-Type if it is not specified at this point
        if(headers['Content-Type'] === undefined) {
            headers['Content-Type'] = config.defaultContentType;
        }

        var signedRequest = {
            method: verb,
            url: url,
            headers: headers,
            data: body
        };
        return axios(signedRequest);
    };

    return awsSigV4Client;
};

/*
 * Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
 
var apiGateway = apiGateway || {};
apiGateway.core = apiGateway.core || {};

apiGateway.core.apiGatewayClientFactory = {};
apiGateway.core.apiGatewayClientFactory.newClient = function (simpleHttpClientConfig, sigV4ClientConfig) {
    var apiGatewayClient = { };
    //Spin up 2 httpClients, one for simple requests, one for SigV4
    var sigV4Client = apiGateway.core.sigV4ClientFactory.newClient(sigV4ClientConfig);
    var simpleHttpClient = apiGateway.core.simpleHttpClientFactory.newClient(simpleHttpClientConfig);

    apiGatewayClient.makeRequest = function (request, authType, additionalParams, apiKey) {
        //Default the request to use the simple http client
        var clientToUse = simpleHttpClient;

        //Attach the apiKey to the headers request if one was provided
        if (apiKey !== undefined && apiKey !== '' && apiKey !== null) {
            request.headers['x-api-key'] = apiKey;
        }

        if (request.body === undefined || request.body === '' || request.body === null || Object.keys(request.body).length === 0) {
            request.body = undefined;
        }

        // If the user specified any additional headers or query params that may not have been modeled
        // merge them into the appropriate request properties
        request.headers = apiGateway.core.utils.mergeInto(request.headers, additionalParams.headers);
        request.queryParams = apiGateway.core.utils.mergeInto(request.queryParams, additionalParams.queryParams);

        //If an auth type was specified inject the appropriate auth client
        if (authType === 'AWS_IAM') {
            clientToUse = sigV4Client;
        }

        //Call the selected http client to make the request, returning a promise once the request is sent
        return clientToUse.makeRequest(request);
    };
    return apiGatewayClient;
};

/*
 * Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
 
var apiGateway = apiGateway || {};
apiGateway.core = apiGateway.core || {};

apiGateway.core.simpleHttpClientFactory = {};
apiGateway.core.simpleHttpClientFactory.newClient = function (config) {
    function buildCanonicalQueryString(queryParams) {
        //Build a properly encoded query string from a QueryParam object
        if (Object.keys(queryParams).length < 1) {
            return '';
        }

        var canonicalQueryString = '';
        for (var property in queryParams) {
            if (queryParams.hasOwnProperty(property)) {
                canonicalQueryString += encodeURIComponent(property) + '=' + encodeURIComponent(queryParams[property]) + '&';
            }
        }

        return canonicalQueryString.substr(0, canonicalQueryString.length - 1);
    }

    var simpleHttpClient = { };
    simpleHttpClient.endpoint = apiGateway.core.utils.assertDefined(config.endpoint, 'endpoint');

    simpleHttpClient.makeRequest = function (request) {
        var verb = apiGateway.core.utils.assertDefined(request.verb, 'verb');
        var path = apiGateway.core.utils.assertDefined(request.path, 'path');
        var queryParams = apiGateway.core.utils.copy(request.queryParams);
        if (queryParams === undefined) {
            queryParams = {};
        }
        var headers = apiGateway.core.utils.copy(request.headers);
        if (headers === undefined) {
            headers = {};
        }

        //If the user has not specified an override for Content type the use default
        if(headers['Content-Type'] === undefined) {
            headers['Content-Type'] = config.defaultContentType;
        }

        //If the user has not specified an override for Accept type the use default
        if(headers['Accept'] === undefined) {
            headers['Accept'] = config.defaultAcceptType;
        }

        var body = apiGateway.core.utils.copy(request.body);
        if (body === undefined) {
            body = '';
        }

        var url = config.endpoint + path;
        var queryString = buildCanonicalQueryString(queryParams);
        if (queryString != '') {
            url += '?' + queryString;
        }
        var simpleHttpRequest = {
            method: verb,
            url: url,
            headers: headers,
            data: body
        };
        return axios(simpleHttpRequest);
    };
    return simpleHttpClient;
};
/*
 * Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */
 
var apiGateway = apiGateway || {};
apiGateway.core = apiGateway.core || {};

apiGateway.core.utils = {
    assertDefined: function (object, name) {
        if (object === undefined) {
            throw name + ' must be defined';
        } else {
            return object;
        }
    },
    assertParametersDefined: function (params, keys, ignore) {
        if (keys === undefined) {
            return;
        }
        if (keys.length > 0 && params === undefined) {
            params = {};
        }
        for (var i = 0; i < keys.length; i++) {
            if(!apiGateway.core.utils.contains(ignore, keys[i])) {
                apiGateway.core.utils.assertDefined(params[keys[i]], keys[i]);
            }
        }
    },
    parseParametersToObject: function (params, keys) {
        if (params === undefined) {
            return {};
        }
        var object = { };
        for (var i = 0; i < keys.length; i++) {
            object[keys[i]] = params[keys[i]];
        }
        return object;
    },
    contains: function(a, obj) {
        if(a === undefined) { return false;}
        var i = a.length;
        while (i--) {
            if (a[i] === obj) {
                return true;
            }
        }
        return false;
    },
    copy: function (obj) {
        if (null == obj || "object" != typeof obj) return obj;
        var copy = obj.constructor();
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
        }
        return copy;
    },
    mergeInto: function (baseObj, additionalProps) {
        if (null == baseObj || "object" != typeof baseObj) return baseObj;
        var merged = baseObj.constructor();
        for (var attr in baseObj) {
            if (baseObj.hasOwnProperty(attr)) merged[attr] = baseObj[attr];
        }
        if (null == additionalProps || "object" != typeof additionalProps) return baseObj;
        for (attr in additionalProps) {
            if (additionalProps.hasOwnProperty(attr)) merged[attr] = additionalProps[attr];
        }
        return merged;
    }
};

/*
 * Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

var apigClientFactory = {};
apigClientFactory.newClient = function (config) {
    var apigClient = { };
    if(config === undefined) {
        config = {
            accessKey: '',
            secretKey: '',
            sessionToken: '',
            region: '',
            apiKey: undefined,
            defaultContentType: 'application/json',
            defaultAcceptType: 'application/json'
        };
    }
    if(config.accessKey === undefined) {
        config.accessKey = '';
    }
    if(config.secretKey === undefined) {
        config.secretKey = '';
    }
    if(config.apiKey === undefined) {
        config.apiKey = '';
    }
    if(config.sessionToken === undefined) {
        config.sessionToken = '';
    }
    if(config.region === undefined) {
        config.region = 'us-east-1';
    }
    //If defaultContentType is not defined then default to application/json
    if(config.defaultContentType === undefined) {
        config.defaultContentType = 'application/json';
    }
    //If defaultAcceptType is not defined then default to application/json
    if(config.defaultAcceptType === undefined) {
        config.defaultAcceptType = 'application/json';
    }

    
    // extract endpoint and path from url
    var invokeUrl = 'https://api.intofilm.org/stage/v2';
    var endpoint = /(^https?:\/\/[^\/]+)/g.exec(invokeUrl)[1];
    var pathComponent = invokeUrl.substring(endpoint.length);

    var sigV4ClientConfig = {
        accessKey: config.accessKey,
        secretKey: config.secretKey,
        sessionToken: config.sessionToken,
        serviceName: 'execute-api',
        region: config.region,
        endpoint: endpoint,
        defaultContentType: config.defaultContentType,
        defaultAcceptType: config.defaultAcceptType
    };

    var authType = 'NONE';
    if (sigV4ClientConfig.accessKey !== undefined && sigV4ClientConfig.accessKey !== '' && sigV4ClientConfig.secretKey !== undefined && sigV4ClientConfig.secretKey !== '') {
        authType = 'AWS_IAM';
    }

    var simpleHttpClientConfig = {
        endpoint: endpoint,
        defaultContentType: config.defaultContentType,
        defaultAcceptType: config.defaultAcceptType
    };

    var apiGatewayClient = apiGateway.core.apiGatewayClientFactory.newClient(simpleHttpClientConfig, sigV4ClientConfig);
    
    
    
    apigClient.addressesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['limit', 'postcode', 'company_name', 'type', 'offset'], ['body']);
        
        var addressesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/addresses').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['limit', 'postcode', 'company_name', 'type', 'offset']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(addressesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.addressesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var addressesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/addresses').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(addressesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.addressesTypeGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['type'], ['body']);
        
        var addressesTypeGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/addresses/{type}').expand(apiGateway.core.utils.parseParametersToObject(params, ['type'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(addressesTypeGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.addressesTypeOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var addressesTypeOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/addresses/{type}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(addressesTypeOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.addressesTypeIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['type', 'id'], ['body']);
        
        var addressesTypeIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/addresses/{type}/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['type', 'id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(addressesTypeIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.addressesTypeIdPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['type', 'id', 'body'], ['body']);
        
        var addressesTypeIdPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/addresses/{type}/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['type', 'id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(addressesTypeIdPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.addressesTypeIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var addressesTypeIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/addresses/{type}/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(addressesTypeIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.addressesTypeIdDisablePatch = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['type', 'id'], ['body']);
        
        var addressesTypeIdDisablePatchRequest = {
            verb: 'patch'.toUpperCase(),
            path: pathComponent + uritemplate('/addresses/{type}/{id}/disable').expand(apiGateway.core.utils.parseParametersToObject(params, ['type', 'id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(addressesTypeIdDisablePatchRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.addressesTypeIdDisableOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var addressesTypeIdDisableOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/addresses/{type}/{id}/disable').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(addressesTypeIdDisableOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.addressesTypeIdHistoryGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['type', 'id'], ['body']);
        
        var addressesTypeIdHistoryGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/addresses/{type}/{id}/history').expand(apiGateway.core.utils.parseParametersToObject(params, ['type', 'id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(addressesTypeIdHistoryGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.addressesTypeIdHistoryOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var addressesTypeIdHistoryOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/addresses/{type}/{id}/history').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(addressesTypeIdHistoryOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubAnnouncementsGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['created--sort', 'modified--to', 'status', 'created--from', 'id', 'id--in', 'status--in', 'limit', 'club_id', 'offset', 'modified', 'modified--operator', 'created', 'modified--from', 'created--operator', 'modified--sort', 'created--to'], ['body']);
        
        var clubAnnouncementsGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/club-announcements').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['created--sort', 'modified--to', 'status', 'created--from', 'id', 'id--in', 'status--in', 'limit', 'club_id', 'offset', 'modified', 'modified--operator', 'created', 'modified--from', 'created--operator', 'modified--sort', 'created--to']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubAnnouncementsGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubAnnouncementsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var clubAnnouncementsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/club-announcements').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubAnnouncementsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubAnnouncementsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var clubAnnouncementsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/club-announcements').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubAnnouncementsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubAnnouncementsIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var clubAnnouncementsIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/club-announcements/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubAnnouncementsIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubAnnouncementsIdDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var clubAnnouncementsIdDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/club-announcements/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubAnnouncementsIdDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubAnnouncementsIdPatch = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var clubAnnouncementsIdPatchRequest = {
            verb: 'patch'.toUpperCase(),
            path: pathComponent + uritemplate('/club-announcements/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubAnnouncementsIdPatchRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubAnnouncementsIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var clubAnnouncementsIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/club-announcements/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubAnnouncementsIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['created--sort', 'property_value', 'limit', 'offset', 'created', 'created--operator', 'status', 'club_code', 'user_hash', 'property_name', 'created--from', 'created--to'], ['body']);
        
        var clubsGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['created--sort', 'property_value', 'limit', 'offset', 'created', 'created--operator', 'status', 'club_code', 'user_hash', 'property_name', 'created--from', 'created--to']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var clubsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var clubsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsDeliverableGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['offset', 'limit'], ['body']);
        
        var clubsDeliverableGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/deliverable').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['offset', 'limit']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsDeliverableGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsDeliverableOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var clubsDeliverableOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/deliverable').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsDeliverableOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var clubsIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdPatch = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var clubsIdPatchRequest = {
            verb: 'patch'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdPatchRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var clubsIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdOrderingEligibilityGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var clubsIdOrderingEligibilityGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}/ordering-eligibility').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdOrderingEligibilityGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdOrderingEligibilityOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var clubsIdOrderingEligibilityOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}/ordering-eligibility').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdOrderingEligibilityOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdPropertiesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var clubsIdPropertiesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdPropertiesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdPropertiesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var clubsIdPropertiesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdPropertiesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdPropertiesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var clubsIdPropertiesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdPropertiesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdPropertiesNamePut = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'id', 'body'], ['body']);
        
        var clubsIdPropertiesNamePutRequest = {
            verb: 'put'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdPropertiesNamePutRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdPropertiesNameDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'id'], ['body']);
        
        var clubsIdPropertiesNameDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdPropertiesNameDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdPropertiesNameOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var clubsIdPropertiesNameOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdPropertiesNameOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdStatisticsGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var clubsIdStatisticsGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}/statistics').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdStatisticsGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdStatisticsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var clubsIdStatisticsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}/statistics').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdStatisticsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdStatisticsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var clubsIdStatisticsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}/statistics').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdStatisticsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdUsersGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'created--sort', 'limit', 'offset', 'created', 'role--sort', 'role'], ['body']);
        
        var clubsIdUsersGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}/users').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['created--sort', 'limit', 'offset', 'created', 'role--sort', 'role']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdUsersGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdUsersPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var clubsIdUsersPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}/users').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdUsersPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdUsersDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var clubsIdUsersDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}/users').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdUsersDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.clubsIdUsersOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var clubsIdUsersOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/clubs/{id}/users').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(clubsIdUsersOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['created--sort', 'email', 'limit', 'token', 'offset', 'created', 'created--operator', 'subscription', 'status', 'created--from', 'created--to'], ['body']);
        
        var contactsGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['created--sort', 'email', 'limit', 'token', 'offset', 'created', 'created--operator', 'subscription', 'status', 'created--from', 'created--to']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var contactsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var contactsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsXmlGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['created--sort', 'token--sort', 'email', 'limit', 'token', 'offset', 'created', 'created--operator', 'subscription', 'status', 'created--from', 'created--to'], ['body']);
        
        var contactsXmlGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts.xml').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['created--sort', 'token--sort', 'email', 'limit', 'token', 'offset', 'created', 'created--operator', 'subscription', 'status', 'created--from', 'created--to']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsXmlGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsXmlOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var contactsXmlOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts.xml').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsXmlOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var contactsIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsIdPut = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var contactsIdPutRequest = {
            verb: 'put'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsIdPutRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsIdPatch = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var contactsIdPatchRequest = {
            verb: 'patch'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsIdPatchRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var contactsIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsIdActivatePatch = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var contactsIdActivatePatchRequest = {
            verb: 'patch'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts/{id}/activate').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsIdActivatePatchRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsIdActivateOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var contactsIdActivateOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts/{id}/activate').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsIdActivateOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsIdBlacklistPatch = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var contactsIdBlacklistPatchRequest = {
            verb: 'patch'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts/{id}/blacklist').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsIdBlacklistPatchRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsIdBlacklistOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var contactsIdBlacklistOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts/{id}/blacklist').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsIdBlacklistOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsIdSubscriptionsSubscriptionIdPut = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['subscription_id', 'id'], ['body']);
        
        var contactsIdSubscriptionsSubscriptionIdPutRequest = {
            verb: 'put'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts/{id}/subscriptions/{subscription_id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['subscription_id', 'id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsIdSubscriptionsSubscriptionIdPutRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsIdSubscriptionsSubscriptionIdDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['subscription_id', 'id'], ['body']);
        
        var contactsIdSubscriptionsSubscriptionIdDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts/{id}/subscriptions/{subscription_id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['subscription_id', 'id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsIdSubscriptionsSubscriptionIdDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsIdSubscriptionsSubscriptionIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var contactsIdSubscriptionsSubscriptionIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts/{id}/subscriptions/{subscription_id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsIdSubscriptionsSubscriptionIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsIdTokenPatch = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var contactsIdTokenPatchRequest = {
            verb: 'patch'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts/{id}/token').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsIdTokenPatchRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.contactsIdTokenOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var contactsIdTokenOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/contacts/{id}/token').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(contactsIdTokenOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.crmLogsGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['status', 'limit', 'modified--sort', 'offset', 'status--sort'], ['body']);
        
        var crmLogsGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/crm-logs').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['status', 'limit', 'modified--sort', 'offset', 'status--sort']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(crmLogsGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.crmLogsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var crmLogsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/crm-logs').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(crmLogsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.crmLogsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var crmLogsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/crm-logs').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(crmLogsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.crmLogsSignatureGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['signature'], ['body']);
        
        var crmLogsSignatureGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/crm-logs/{signature}').expand(apiGateway.core.utils.parseParametersToObject(params, ['signature'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(crmLogsSignatureGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.crmLogsSignaturePost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['signature', 'body'], ['body']);
        
        var crmLogsSignaturePostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/crm-logs/{signature}').expand(apiGateway.core.utils.parseParametersToObject(params, ['signature', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(crmLogsSignaturePostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.crmLogsSignatureOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var crmLogsSignatureOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/crm-logs/{signature}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(crmLogsSignatureOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.diskCopiesIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var diskCopiesIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/disk-copies/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(diskCopiesIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.diskCopiesIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var diskCopiesIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/disk-copies/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(diskCopiesIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.diskCopiesIdMarkBlurayPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var diskCopiesIdMarkBlurayPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/disk-copies/{id}/mark-bluray').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(diskCopiesIdMarkBlurayPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.diskCopiesIdMarkBlurayOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var diskCopiesIdMarkBlurayOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/disk-copies/{id}/mark-bluray').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(diskCopiesIdMarkBlurayOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.diskCopiesIdMarkDamagedPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var diskCopiesIdMarkDamagedPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/disk-copies/{id}/mark-damaged').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(diskCopiesIdMarkDamagedPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.diskCopiesIdMarkDamagedOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var diskCopiesIdMarkDamagedOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/disk-copies/{id}/mark-damaged').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(diskCopiesIdMarkDamagedOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.distributorsGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['offset', 'limit'], ['body']);
        
        var distributorsGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/distributors').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['offset', 'limit']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(distributorsGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.distributorsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var distributorsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/distributors').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(distributorsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.educationPhasesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['limit', 'name--sort', 'offset', 'name', 'id'], ['body']);
        
        var educationPhasesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/education-phases').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['limit', 'name--sort', 'offset', 'name', 'id']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(educationPhasesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.educationPhasesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var educationPhasesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/education-phases').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(educationPhasesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.externalAddressesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['postcode', 'offset', 'limit'], ['body']);
        
        var externalAddressesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/external-addresses').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['postcode', 'offset', 'limit']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(externalAddressesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.externalAddressesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var externalAddressesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/external-addresses').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(externalAddressesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.externalAddressesIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var externalAddressesIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/external-addresses/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(externalAddressesIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.externalAddressesIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var externalAddressesIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/external-addresses/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(externalAddressesIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmObtainableIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var filmObtainableIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/film-obtainable/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmObtainableIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmObtainableIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var filmObtainableIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/film-obtainable/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmObtainableIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmOrdersGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['user_id', 'created--sort', 'status--in', 'property_value', 'limit', 'club_id', 'film_id', 'distributor_id', 'offset', 'status', 'property_name'], ['body']);
        
        var filmOrdersGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/film-orders').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['user_id', 'created--sort', 'status--in', 'property_value', 'limit', 'club_id', 'film_id', 'distributor_id', 'offset', 'status', 'property_name']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmOrdersGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmOrdersPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var filmOrdersPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/film-orders').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmOrdersPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmOrdersOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var filmOrdersOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/film-orders').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmOrdersOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmOrdersIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var filmOrdersIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/film-orders/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmOrdersIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmOrdersIdDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var filmOrdersIdDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/film-orders/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmOrdersIdDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmOrdersIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var filmOrdersIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/film-orders/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmOrdersIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmOrdersIdLostPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var filmOrdersIdLostPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/film-orders/{id}/lost').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmOrdersIdLostPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmOrdersIdLostOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var filmOrdersIdLostOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/film-orders/{id}/lost').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmOrdersIdLostOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmOrdersIdPriorityPut = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var filmOrdersIdPriorityPutRequest = {
            verb: 'put'.toUpperCase(),
            path: pathComponent + uritemplate('/film-orders/{id}/priority').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmOrdersIdPriorityPutRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmOrdersIdPriorityOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var filmOrdersIdPriorityOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/film-orders/{id}/priority').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmOrdersIdPriorityOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmOrdersIdPropertiesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var filmOrdersIdPropertiesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/film-orders/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmOrdersIdPropertiesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmOrdersIdPropertiesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var filmOrdersIdPropertiesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/film-orders/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmOrdersIdPropertiesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmOrdersIdPropertiesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var filmOrdersIdPropertiesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/film-orders/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmOrdersIdPropertiesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmOrdersIdPropertiesNamePut = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'id', 'body'], ['body']);
        
        var filmOrdersIdPropertiesNamePutRequest = {
            verb: 'put'.toUpperCase(),
            path: pathComponent + uritemplate('/film-orders/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmOrdersIdPropertiesNamePutRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmOrdersIdPropertiesNameDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'id'], ['body']);
        
        var filmOrdersIdPropertiesNameDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/film-orders/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmOrdersIdPropertiesNameDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmOrdersIdPropertiesNameOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var filmOrdersIdPropertiesNameOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/film-orders/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmOrdersIdPropertiesNameOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmRankTaxonomyGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['taxonomy', 'offset', 'limit'], ['body']);
        
        var filmRankTaxonomyGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/film-rank/{taxonomy}').expand(apiGateway.core.utils.parseParametersToObject(params, ['taxonomy', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['offset', 'limit']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmRankTaxonomyGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmRankTaxonomyOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var filmRankTaxonomyOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/film-rank/{taxonomy}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmRankTaxonomyOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmWishlistsGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['created--sort', 'limit', 'film_id', 'club_id', 'offset', 'created', 'created--operator', 'id--sort', 'user_role', 'user_hash'], ['body']);
        
        var filmWishlistsGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/film-wishlists').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['created--sort', 'limit', 'film_id', 'club_id', 'offset', 'created', 'created--operator', 'id--sort', 'user_role', 'user_hash']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmWishlistsGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmWishlistsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var filmWishlistsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/film-wishlists').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmWishlistsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmWishlistsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var filmWishlistsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/film-wishlists').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmWishlistsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmWishlistsIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var filmWishlistsIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/film-wishlists/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmWishlistsIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmWishlistsIdDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var filmWishlistsIdDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/film-wishlists/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmWishlistsIdDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmWishlistsIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var filmWishlistsIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/film-wishlists/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmWishlistsIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmsGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['created--sort', 'title--sort', 'year--sort', 'modified--to', 'taxonomy--in', 'director--in', 'year--to', 'certificate--sort', 'synopsis--operator', 'title', 'id', 'id--in', 'summary', 'year', 'festival_duration--operator', 'limit', 'created', 'year--operator', 'festival_duration--sort', 'modified--from', 'modified--sort', 'summary--operator', 'active', 'festival_duration', 'duration', 'actor--in', 'id--sort', 'duration--sort', 'duration--operator', 'created--from', 'active--sort', 'certificate', 'offset', 'modified', 'modified--operator', 'created--operator', 'synopsis', 'created--to', 'title--operator', 'year--from'], ['body']);
        
        var filmsGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/films').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['created--sort', 'title--sort', 'year--sort', 'modified--to', 'taxonomy--in', 'director--in', 'year--to', 'certificate--sort', 'synopsis--operator', 'title', 'id', 'id--in', 'summary', 'year', 'festival_duration--operator', 'limit', 'created', 'year--operator', 'festival_duration--sort', 'modified--from', 'modified--sort', 'summary--operator', 'active', 'festival_duration', 'duration', 'actor--in', 'id--sort', 'duration--sort', 'duration--operator', 'created--from', 'active--sort', 'certificate', 'offset', 'modified', 'modified--operator', 'created--operator', 'synopsis', 'created--to', 'title--operator', 'year--from']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmsGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var filmsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/films').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmsIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var filmsIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/films/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmsIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmsIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var filmsIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/films/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmsIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmsIdRatingsGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var filmsIdRatingsGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/films/{id}/ratings').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmsIdRatingsGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmsIdRatingsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var filmsIdRatingsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/films/{id}/ratings').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmsIdRatingsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmsIdRatingsUserHashGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['user_hash', 'id'], ['body']);
        
        var filmsIdRatingsUserHashGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/films/{id}/ratings/{user_hash}').expand(apiGateway.core.utils.parseParametersToObject(params, ['user_hash', 'id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmsIdRatingsUserHashGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmsIdRatingsUserHashPut = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['user_hash', 'id', 'body'], ['body']);
        
        var filmsIdRatingsUserHashPutRequest = {
            verb: 'put'.toUpperCase(),
            path: pathComponent + uritemplate('/films/{id}/ratings/{user_hash}').expand(apiGateway.core.utils.parseParametersToObject(params, ['user_hash', 'id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmsIdRatingsUserHashPutRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmsIdRatingsUserHashOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var filmsIdRatingsUserHashOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/films/{id}/ratings/{user_hash}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmsIdRatingsUserHashOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmsIdRelatedGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var filmsIdRelatedGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/films/{id}/related').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmsIdRelatedGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmsIdRelatedOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var filmsIdRelatedOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/films/{id}/related').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmsIdRelatedOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmsIdTaxonomiesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var filmsIdTaxonomiesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/films/{id}/taxonomies').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmsIdTaxonomiesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.filmsIdTaxonomiesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var filmsIdTaxonomiesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/films/{id}/taxonomies').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(filmsIdTaxonomiesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['property_value', 'limit', 'offset', 'created', 'modified', 'property_name', 'id'], ['body']);
        
        var messagesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/messages').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['property_value', 'limit', 'offset', 'created', 'modified', 'property_name', 'id']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var messagesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/messages').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var messagesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/messages').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var messagesIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/messages/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesIdPatch = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var messagesIdPatchRequest = {
            verb: 'patch'.toUpperCase(),
            path: pathComponent + uritemplate('/messages/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesIdPatchRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var messagesIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/messages/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesIdFiltersPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var messagesIdFiltersPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/messages/{id}/filters').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesIdFiltersPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesIdFiltersOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var messagesIdFiltersOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/messages/{id}/filters').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesIdFiltersOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesIdFiltersFilterIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['filter_id', 'id'], ['body']);
        
        var messagesIdFiltersFilterIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/messages/{id}/filters/{filter_id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['filter_id', 'id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesIdFiltersFilterIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesIdFiltersFilterIdDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['filter_id', 'id'], ['body']);
        
        var messagesIdFiltersFilterIdDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/messages/{id}/filters/{filter_id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['filter_id', 'id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesIdFiltersFilterIdDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesIdFiltersFilterIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var messagesIdFiltersFilterIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/messages/{id}/filters/{filter_id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesIdFiltersFilterIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesIdPropertiesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var messagesIdPropertiesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/messages/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesIdPropertiesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesIdPropertiesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var messagesIdPropertiesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/messages/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesIdPropertiesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesIdPropertiesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var messagesIdPropertiesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/messages/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesIdPropertiesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesIdPropertiesNamePut = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'id', 'body'], ['body']);
        
        var messagesIdPropertiesNamePutRequest = {
            verb: 'put'.toUpperCase(),
            path: pathComponent + uritemplate('/messages/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesIdPropertiesNamePutRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesIdPropertiesNameDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'id'], ['body']);
        
        var messagesIdPropertiesNameDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/messages/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesIdPropertiesNameDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.messagesIdPropertiesNameOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var messagesIdPropertiesNameOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/messages/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(messagesIdPropertiesNameOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.orderItemsIdPatch = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var orderItemsIdPatchRequest = {
            verb: 'patch'.toUpperCase(),
            path: pathComponent + uritemplate('/order-items/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(orderItemsIdPatchRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.orderItemsIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var orderItemsIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/order-items/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(orderItemsIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.orderItemsIdPropertiesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var orderItemsIdPropertiesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/order-items/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(orderItemsIdPropertiesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.orderItemsIdPropertiesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var orderItemsIdPropertiesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/order-items/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(orderItemsIdPropertiesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.orderItemsIdPropertiesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var orderItemsIdPropertiesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/order-items/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(orderItemsIdPropertiesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.orderItemsIdPropertiesNamePut = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'id', 'body'], ['body']);
        
        var orderItemsIdPropertiesNamePutRequest = {
            verb: 'put'.toUpperCase(),
            path: pathComponent + uritemplate('/order-items/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(orderItemsIdPropertiesNamePutRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.orderItemsIdPropertiesNameDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'id'], ['body']);
        
        var orderItemsIdPropertiesNameDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/order-items/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(orderItemsIdPropertiesNameDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.orderItemsIdPropertiesNameOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var orderItemsIdPropertiesNameOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/order-items/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(orderItemsIdPropertiesNameOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.ordersGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['item_id', 'problem_property_value--operator', 'created--sort', 'queued--sort', 'disk_orderable', 'film_orderable', 'lovefilm_id', 'distributor_allocation_id', 'distributor_allocation_id--operator', 'status', 'status--in', 'property_value', 'out--sort', 'limit', 'club_id', 'archived--sort', 'damaged--sort', 'modified--sort', 'disk_copy_status', 'distributor_id', 'ordered--sort', 'problem_property_value--name', 'returned--sort', 'distributor_copy_id--operator', 'problem_property_value', 'lost--sort', 'offset', 'problem_property_value--value', 'property_name', 'distributor_copy_id'], ['body']);
        
        var ordersGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/orders').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['item_id', 'problem_property_value--operator', 'created--sort', 'queued--sort', 'disk_orderable', 'film_orderable', 'lovefilm_id', 'distributor_allocation_id', 'distributor_allocation_id--operator', 'status', 'status--in', 'property_value', 'out--sort', 'limit', 'club_id', 'archived--sort', 'damaged--sort', 'modified--sort', 'disk_copy_status', 'distributor_id', 'ordered--sort', 'problem_property_value--name', 'returned--sort', 'distributor_copy_id--operator', 'problem_property_value', 'lost--sort', 'offset', 'problem_property_value--value', 'property_name', 'distributor_copy_id']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(ordersGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.ordersPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var ordersPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/orders').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(ordersPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.ordersOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var ordersOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/orders').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(ordersOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.ordersStatusPatch = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var ordersStatusPatchRequest = {
            verb: 'patch'.toUpperCase(),
            path: pathComponent + uritemplate('/orders/status').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(ordersStatusPatchRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.ordersStatusOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var ordersStatusOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/orders/status').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(ordersStatusOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.ordersIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var ordersIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/orders/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(ordersIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.ordersIdDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var ordersIdDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/orders/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(ordersIdDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.ordersIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var ordersIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/orders/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(ordersIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.ordersIdPropertiesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var ordersIdPropertiesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/orders/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(ordersIdPropertiesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.ordersIdPropertiesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var ordersIdPropertiesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/orders/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(ordersIdPropertiesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.ordersIdPropertiesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var ordersIdPropertiesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/orders/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(ordersIdPropertiesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.ordersIdPropertiesNamePut = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'id', 'body'], ['body']);
        
        var ordersIdPropertiesNamePutRequest = {
            verb: 'put'.toUpperCase(),
            path: pathComponent + uritemplate('/orders/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(ordersIdPropertiesNamePutRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.ordersIdPropertiesNameDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'id'], ['body']);
        
        var ordersIdPropertiesNameDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/orders/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(ordersIdPropertiesNameDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.ordersIdPropertiesNameOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var ordersIdPropertiesNameOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/orders/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(ordersIdPropertiesNameOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.ordersIdStatusPatch = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var ordersIdStatusPatchRequest = {
            verb: 'patch'.toUpperCase(),
            path: pathComponent + uritemplate('/orders/{id}/status').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(ordersIdStatusPatchRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.ordersIdStatusOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var ordersIdStatusOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/orders/{id}/status').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(ordersIdStatusOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationAgreementsIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var organisationAgreementsIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/organisation-agreements/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationAgreementsIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationAgreementsIdPatch = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var organisationAgreementsIdPatchRequest = {
            verb: 'patch'.toUpperCase(),
            path: pathComponent + uritemplate('/organisation-agreements/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationAgreementsIdPatchRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationAgreementsIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var organisationAgreementsIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/organisation-agreements/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationAgreementsIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationTypesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['order--sort', 'limit', 'name--sort', 'offset', 'name', 'id'], ['body']);
        
        var organisationTypesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/organisation-types').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['order--sort', 'limit', 'name--sort', 'offset', 'name', 'id']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationTypesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationTypesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var organisationTypesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/organisation-types').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationTypesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationsGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['property_value', 'limit', 'offset', 'name', 'name--sort', 'property_name', 'id'], ['body']);
        
        var organisationsGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/organisations').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['property_value', 'limit', 'offset', 'name', 'name--sort', 'property_name', 'id']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationsGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var organisationsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/organisations').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var organisationsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/organisations').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationsIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var organisationsIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/organisations/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationsIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationsIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var organisationsIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/organisations/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationsIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationsIdAgreementsGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'offset', 'limit'], ['body']);
        
        var organisationsIdAgreementsGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/organisations/{id}/agreements').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['offset', 'limit']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationsIdAgreementsGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationsIdAgreementsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var organisationsIdAgreementsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/organisations/{id}/agreements').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationsIdAgreementsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationsIdAgreementsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var organisationsIdAgreementsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/organisations/{id}/agreements').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationsIdAgreementsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationsIdPropertiesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var organisationsIdPropertiesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/organisations/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationsIdPropertiesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationsIdPropertiesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var organisationsIdPropertiesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/organisations/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationsIdPropertiesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationsIdPropertiesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var organisationsIdPropertiesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/organisations/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationsIdPropertiesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationsIdPropertiesNamePut = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'id', 'body'], ['body']);
        
        var organisationsIdPropertiesNamePutRequest = {
            verb: 'put'.toUpperCase(),
            path: pathComponent + uritemplate('/organisations/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationsIdPropertiesNamePutRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationsIdPropertiesNameDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'id'], ['body']);
        
        var organisationsIdPropertiesNameDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/organisations/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationsIdPropertiesNameDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.organisationsIdPropertiesNameOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var organisationsIdPropertiesNameOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/organisations/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(organisationsIdPropertiesNameOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.reviewsGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['created--sort', 'rotw--sort', 'published--sort', 'film_id', 'status', 'rotw_date--sort', 'user_hash', 'rotw', 'id', 'film_status', 'property_value', 'limit', 'club_id', 'modified', 'created', 'offset', 'featured', 'draft--operator', 'created--operator', 'modified--sort', 'property_name', 'draft', 'draft--sort'], ['body']);
        
        var reviewsGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/reviews').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['created--sort', 'rotw--sort', 'published--sort', 'film_id', 'status', 'rotw_date--sort', 'user_hash', 'rotw', 'id', 'film_status', 'property_value', 'limit', 'club_id', 'modified', 'created', 'offset', 'featured', 'draft--operator', 'created--operator', 'modified--sort', 'property_name', 'draft', 'draft--sort']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(reviewsGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.reviewsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var reviewsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/reviews').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(reviewsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.reviewsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var reviewsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/reviews').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(reviewsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.reviewsIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var reviewsIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/reviews/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(reviewsIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.reviewsIdDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var reviewsIdDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/reviews/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(reviewsIdDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.reviewsIdPatch = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var reviewsIdPatchRequest = {
            verb: 'patch'.toUpperCase(),
            path: pathComponent + uritemplate('/reviews/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(reviewsIdPatchRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.reviewsIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var reviewsIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/reviews/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(reviewsIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.reviewsIdPropertiesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var reviewsIdPropertiesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/reviews/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(reviewsIdPropertiesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.reviewsIdPropertiesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var reviewsIdPropertiesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/reviews/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(reviewsIdPropertiesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.reviewsIdPropertiesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var reviewsIdPropertiesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/reviews/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(reviewsIdPropertiesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.reviewsIdPropertiesNamePut = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'id', 'body'], ['body']);
        
        var reviewsIdPropertiesNamePutRequest = {
            verb: 'put'.toUpperCase(),
            path: pathComponent + uritemplate('/reviews/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(reviewsIdPropertiesNamePutRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.reviewsIdPropertiesNameDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'id'], ['body']);
        
        var reviewsIdPropertiesNameDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/reviews/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(reviewsIdPropertiesNameDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.reviewsIdPropertiesNameOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var reviewsIdPropertiesNameOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/reviews/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(reviewsIdPropertiesNameOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.reviewsIdReportPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var reviewsIdReportPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/reviews/{id}/report').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(reviewsIdReportPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.reviewsIdReportOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var reviewsIdReportOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/reviews/{id}/report').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(reviewsIdReportOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.rolesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['status', 'limit', 'name--sort', 'offset', 'name'], ['body']);
        
        var rolesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/roles').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['status', 'limit', 'name--sort', 'offset', 'name']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(rolesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.rolesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var rolesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/roles').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(rolesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.screeningsGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['created--sort', 'modified--to', 'film_id', 'status', 'date', 'created--from', 'status--in', 'limit', 'club_id', 'offset', 'created', 'modified', 'modified--operator', 'date--operator', 'created--operator', 'modified--from', 'modified--sort', 'date--from', 'date--to', 'created--to'], ['body']);
        
        var screeningsGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/screenings').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['created--sort', 'modified--to', 'film_id', 'status', 'date', 'created--from', 'status--in', 'limit', 'club_id', 'offset', 'created', 'modified', 'modified--operator', 'date--operator', 'created--operator', 'modified--from', 'modified--sort', 'date--from', 'date--to', 'created--to']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(screeningsGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.screeningsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var screeningsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/screenings').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(screeningsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.screeningsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var screeningsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/screenings').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(screeningsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.screeningsIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var screeningsIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/screenings/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(screeningsIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.screeningsIdDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var screeningsIdDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/screenings/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(screeningsIdDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.screeningsIdPatch = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var screeningsIdPatchRequest = {
            verb: 'patch'.toUpperCase(),
            path: pathComponent + uritemplate('/screenings/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(screeningsIdPatchRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.screeningsIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var screeningsIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/screenings/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(screeningsIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.shortUrlsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var shortUrlsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/short-urls').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(shortUrlsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.shortUrlsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var shortUrlsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/short-urls').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(shortUrlsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.subscriptionsGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['offset', 'limit'], ['body']);
        
        var subscriptionsGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/subscriptions').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['offset', 'limit']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(subscriptionsGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.subscriptionsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var subscriptionsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/subscriptions').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(subscriptionsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.taxonomiesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var taxonomiesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/taxonomies').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(taxonomiesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.taxonomiesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var taxonomiesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/taxonomies').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(taxonomiesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.uploadsGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['created--sort', 'filename', 'title', 'property_value', 'limit', 'offset', 'created', 'modified', 'modified--operator', 'tagline', 'modified--sort', 'property_name', 'synopsis'], ['body']);
        
        var uploadsGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/uploads').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['created--sort', 'filename', 'title', 'property_value', 'limit', 'offset', 'created', 'modified', 'modified--operator', 'tagline', 'modified--sort', 'property_name', 'synopsis']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(uploadsGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.uploadsPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var uploadsPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/uploads').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(uploadsPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.uploadsOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var uploadsOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/uploads').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(uploadsOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.uploadsIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var uploadsIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/uploads/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(uploadsIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.uploadsIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var uploadsIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/uploads/{id}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(uploadsIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.uploadsIdDownloadGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var uploadsIdDownloadGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/uploads/{id}/download').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(uploadsIdDownloadGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.uploadsIdDownloadOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var uploadsIdDownloadOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/uploads/{id}/download').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(uploadsIdDownloadOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.uploadsIdPropertiesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id'], ['body']);
        
        var uploadsIdPropertiesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/uploads/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(uploadsIdPropertiesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.uploadsIdPropertiesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['id', 'body'], ['body']);
        
        var uploadsIdPropertiesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/uploads/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(uploadsIdPropertiesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.uploadsIdPropertiesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var uploadsIdPropertiesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/uploads/{id}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(uploadsIdPropertiesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.uploadsIdPropertiesNamePut = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'id', 'body'], ['body']);
        
        var uploadsIdPropertiesNamePutRequest = {
            verb: 'put'.toUpperCase(),
            path: pathComponent + uritemplate('/uploads/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'id', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(uploadsIdPropertiesNamePutRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.uploadsIdPropertiesNameDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'id'], ['body']);
        
        var uploadsIdPropertiesNameDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/uploads/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'id'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(uploadsIdPropertiesNameDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.uploadsIdPropertiesNameOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var uploadsIdPropertiesNameOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/uploads/{id}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(uploadsIdPropertiesNameOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.userMessagesUuidGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['uuid', 'created--sort', 'modified--to', 'expires', 'id', 'created--from', 'expires--to', 'limit', 'offset', 'created', 'modified', 'modified--operator', 'created--operator', 'modified--from', 'expires--operator', 'modified--sort', 'expires--from', 'created--to'], ['body']);
        
        var userMessagesUuidGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/user-messages/{uuid}').expand(apiGateway.core.utils.parseParametersToObject(params, ['uuid', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['created--sort', 'modified--to', 'expires', 'id', 'created--from', 'expires--to', 'limit', 'offset', 'created', 'modified', 'modified--operator', 'created--operator', 'modified--from', 'expires--operator', 'modified--sort', 'expires--from', 'created--to']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(userMessagesUuidGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.userMessagesUuidOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var userMessagesUuidOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/user-messages/{uuid}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(userMessagesUuidOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['uuid--in', 'password_reset_code', 'username', 'property_value', 'limit', 'email', 'username--sort', 'offset', 'property_name', 'invitation_code'], ['body']);
        
        var usersGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/users').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, ['uuid--in', 'password_reset_code', 'username', 'property_value', 'limit', 'email', 'username--sort', 'offset', 'property_name', 'invitation_code']),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var usersPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/users').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var usersOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/users').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersAuthenticatePatch = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['body'], ['body']);
        
        var usersAuthenticatePatchRequest = {
            verb: 'patch'.toUpperCase(),
            path: pathComponent + uritemplate('/users/authenticate').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersAuthenticatePatchRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersAuthenticateOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var usersAuthenticateOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/users/authenticate').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersAuthenticateOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersUuidGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['uuid'], ['body']);
        
        var usersUuidGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/users/{uuid}').expand(apiGateway.core.utils.parseParametersToObject(params, ['uuid'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersUuidGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersUuidPatch = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['uuid', 'body'], ['body']);
        
        var usersUuidPatchRequest = {
            verb: 'patch'.toUpperCase(),
            path: pathComponent + uritemplate('/users/{uuid}').expand(apiGateway.core.utils.parseParametersToObject(params, ['uuid', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersUuidPatchRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersUuidOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var usersUuidOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/users/{uuid}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersUuidOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersUuidIdGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['uuid'], ['body']);
        
        var usersUuidIdGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/users/{uuid}/id').expand(apiGateway.core.utils.parseParametersToObject(params, ['uuid'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersUuidIdGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersUuidIdOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var usersUuidIdOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/users/{uuid}/id').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersUuidIdOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersUuidPropertiesGet = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['uuid'], ['body']);
        
        var usersUuidPropertiesGetRequest = {
            verb: 'get'.toUpperCase(),
            path: pathComponent + uritemplate('/users/{uuid}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['uuid'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersUuidPropertiesGetRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersUuidPropertiesPost = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['uuid', 'body'], ['body']);
        
        var usersUuidPropertiesPostRequest = {
            verb: 'post'.toUpperCase(),
            path: pathComponent + uritemplate('/users/{uuid}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, ['uuid', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersUuidPropertiesPostRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersUuidPropertiesOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var usersUuidPropertiesOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/users/{uuid}/properties').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersUuidPropertiesOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersUuidPropertiesNamePut = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'uuid', 'body'], ['body']);
        
        var usersUuidPropertiesNamePutRequest = {
            verb: 'put'.toUpperCase(),
            path: pathComponent + uritemplate('/users/{uuid}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'uuid', ])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersUuidPropertiesNamePutRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersUuidPropertiesNameDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'uuid'], ['body']);
        
        var usersUuidPropertiesNameDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/users/{uuid}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'uuid'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersUuidPropertiesNameDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersUuidPropertiesNameOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var usersUuidPropertiesNameOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/users/{uuid}/properties/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersUuidPropertiesNameOptionsRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersUuidRolesNamePut = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'uuid'], ['body']);
        
        var usersUuidRolesNamePutRequest = {
            verb: 'put'.toUpperCase(),
            path: pathComponent + uritemplate('/users/{uuid}/roles/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'uuid'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersUuidRolesNamePutRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersUuidRolesNameDelete = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, ['name', 'uuid'], ['body']);
        
        var usersUuidRolesNameDeleteRequest = {
            verb: 'delete'.toUpperCase(),
            path: pathComponent + uritemplate('/users/{uuid}/roles/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, ['name', 'uuid'])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersUuidRolesNameDeleteRequest, authType, additionalParams, config.apiKey);
    };
    
    
    apigClient.usersUuidRolesNameOptions = function (params, body, additionalParams) {
        if(additionalParams === undefined) { additionalParams = {}; }
        
        apiGateway.core.utils.assertParametersDefined(params, [], ['body']);
        
        var usersUuidRolesNameOptionsRequest = {
            verb: 'options'.toUpperCase(),
            path: pathComponent + uritemplate('/users/{uuid}/roles/{name}').expand(apiGateway.core.utils.parseParametersToObject(params, [])),
            headers: apiGateway.core.utils.parseParametersToObject(params, []),
            queryParams: apiGateway.core.utils.parseParametersToObject(params, []),
            body: body
        };
        
        
        return apiGatewayClient.makeRequest(usersUuidRolesNameOptionsRequest, authType, additionalParams, config.apiKey);
    };
    

    return apigClient;
};

/**
 * @param {object} params
 * @param {array} keys
 */
const parseParametersToObject = (params, keys) => {
  var object = {};

  if (params !== undefined) {
    keys.forEach(key => {
      if (params[key]) {
        object[key] = params[key];
      }
    });
  }

  return object;
};

/**
 * @param {object} params
 * @param {array} keys
 * @param {boolean} ignore
 */
const assertParametersDefined = (params, keys, ignore) => {
  return;
};
apiGateway.core.utils.parseParametersToObject = parseParametersToObject;
apiGateway.core.utils.assertParametersDefined = assertParametersDefined;

module.exports = apigClientFactory;
