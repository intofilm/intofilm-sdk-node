# Node INTOFILM SDK

## Usage

```javascript
const sdk = require('./index')({apiKey: '<api_key>', stage: 'stage'});

const upload = sdk.uploadsGet()
  .then((result) => {
    // do stuff
  })
  .catch((result) => {
    console.err(result);
  });
```

## Building the SDKs

The SDKs are built for three environments, development, stage and production. Only production
and stage use  the AWS API Gateway so are provided as is by AWS. The development SDK is a copy
of the stage SDK with the invoke URL replaced with a local URL. It is assumed that the dev API
is identical to the stage API. Interim changes can be made manually but will probably be super
hard to maintain. Enjoy :).

```sh
$ yarn build
<snip>
✨  Done in 3.98s.
```